class MessagesController < ApplicationController

  def create
    @chatroom = Chatroom.find(message_params[:chatroom_id])
    @host = User.find(@chatroom.user_id)
    if current_user == @host
      @chatroom.question += 1
      @chatroom.save
      message = Message.new(message_params)
      message.user = current_user
      message.content = "Question #{@chatroom.question}"
      if message.save
        ActionCable.server.broadcast 'messages',
          message: message.content,
          user: message.user.username
        head :ok
      end
      Message.where(chatroom_id: @chatroom.id, active: true).each do |yeet|
        yeet.update_attributes(active: false)
      end
    else
      if current_user.messages.where(chatroom_id: @chatroom.id, active: true).present? 
      else
        message = Message.new(message_params)
        message.user = current_user
        message.active = true 
        message.save
        message.update_attributes(content: message.created_at.strftime("%A %T.%N"))
        if message.save
          ActionCable.server.broadcast 'messages',
            message: message.content,
            user: message.user.username
          head :ok
        end
      end
    end
  end

  private

    def message_params
      params.require(:message).permit(:content, :chatroom_id)
    end
end
