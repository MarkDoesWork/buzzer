

Sophie DeBenedetto's action cable demo Chatty is the foundation of this really janky rails app. My friend who hosts a trivia style game show needed a web solution for a game-show-style-buzzer.
 
Currently live on heroku: https://buzzer-cloud.herokuapp.com/

### Running Locally

You'll need:

* Ruby 2.3.0
* Postgres
* Redis

post clone stuff:

 `bundle install`
 `rake db:create; rake db:migrate`


