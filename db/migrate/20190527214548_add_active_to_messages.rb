class AddActiveToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :active, :boolean
  end
end
