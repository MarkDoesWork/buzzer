class AddMoreToChatroom < ActiveRecord::Migration[5.0]
  def change
    add_reference :chatrooms, :user, foreign_key: true
    add_column :chatrooms, :question, :integer
  end
end
